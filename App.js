import React from 'react';
import {
  StyleSheet,
  View,
  ImageBackground,
  Text,
  KeyboardAvoidingView,
  Platform,
  StatusBar,
  ActivityIndicator,
} from 'react-native';


import SearchInput from './components/SearchInput';
import getImageForWeather from './utils/getImageForWeather';
import {fetchLocationId,fetchWeather} from './utils/api';

export default class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
       loading:false,
       error:false,
       location: '',
       temperature:0,
       weather:'',
    };

  }

  componentDidMount(){
    this.handleNewLocation('Lahore');
  }

  handleNewLocation = async userInputLocation => {
     this.setState({loading:true}, async() => {
       try{

         const locationId = await fetchLocationId(userInputLocation);
         const { location, weather, temperature} = await fetchWeather(locationId);

         this.setState({
            loading:false,
            error:false,
            location,
            temperature,
            weather,
         })

       }catch(e){
          this.setState({
              loading:false,
              error:true,
          });
       }
     });
  }



  renderErrorView(){
    return(
      <Text style={[styles.smallText, styles.textStyle]}> Could not load weather, please try a different city. </Text>
    );
  }

  renderSuccessView(){
    const{location,weather,temperature} = this.state;
    return(
      <View>
         <Text style={[styles.largeText, styles.textStyle]}> {location}</Text>
         <Text style={[styles.smallText, styles.textStyle]}>  {weather}  </Text>
         <Text style={[styles.largeText, styles.textStyle]}>{`${Math.round(temperature)}°`}</Text>
      </View>
    );
  }

  renderCentralContent(){
     const {error} = this.state

     return(
       <View>
          {error && this.renderErrorView()}
          {!error && this.renderSuccessView()}
        </View>
     );
  }

  render() {

    return (
      <KeyboardAvoidingView style={styles.container} >
      <StatusBar barStyle = 'light-content' />
      <ImageBackground
          source={getImageForWeather(this.state.weather)} style={styles.imageContainer}
          imageStyle={styles.image}  >

        <View style={styles.detailsContainer}>
             <ActivityIndicator animating={this.state.loading} color='white' size='large' />
             {!this.state.loading && this.renderCentralContent()}
            <SearchInput placeholder="Search any city"
                //onSubmit = {this.handleNewLocation} 
                 />
        </View>

      </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#34495E',
  },
  imageContainer: {
    flex: 1,
  },

  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode:'cover'
   },

   detailsContainer:{
     flex:1,
     justifyContent:'center',
     backgroundColor:'rgba(0,0,0,0.2)',
     paddingHorizontal:20,
   },

  textStyle: {
    textAlign: 'center',
    fontFamily: Platform.OS === 'ios' ? 'AvenirNext-Regular' : 'Roboto',
    color: 'white',
  },
  largeText: {
    fontSize: 44,
  },
  smallText: {
    fontSize: 18,
  },
});
